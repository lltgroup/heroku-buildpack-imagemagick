# Heroku Buildpack for ImageMagick

This is a [Heroku buildpack](http://devcenter.heroku.com/articles/buildpacks) for vendoring the ImageMagick binaries into your project. It should be used
for Rails 5+ projects running Heroku v16+.

_This project is currently being maintained by [LLT Group](https://llt-group.com)_
